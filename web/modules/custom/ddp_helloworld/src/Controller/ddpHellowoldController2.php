<?php

namespace Drupal\ddp_helloworld\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for ddp_helloworld routes.
 */
class ddpHellowoldController2 extends ControllerBase {

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $account;

  /**
   * The controller constructor.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The current user.
   */
  public function __construct(AccountInterface $account) {
    $this->account = $account;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user')
    );
  }

  /**
   * Builds the response.
   */
  public function build() {

    $current_user = $this->account;
    $user = $current_user -> getAccountName();
    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('Hola @name', ['@name' => $user]),
    ];

    return $build;
  }

}
