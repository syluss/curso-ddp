<?php

namespace Drupal\ddp_pages\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for Ddp pages routes.
 */
class DdpPagesController extends ControllerBase {

  /**
   * Builds the response.
   */
  public function calculator($num1, $num2) {
    $suma = $num1 + $num2;
    $resta = $num1 - $num2;
    $multi = $num1 * $num2;
    $divi = $num1 / $num2;

    $items = [
      $num1 . ' + ' . $num2 . ' = ' . $suma,
      $num1 . ' - ' . $num2 . ' = ' . $resta,
      $num1 . ' * ' . $num2 . ' = ' . $multi,
      $num1 . ' / ' . $num2 . ' = ' . $divi,
    ];

    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
      '#items' => $items,
    ];

    return $build;
  }

  public function user($nid) {
    $list = ['A', 'B'];

    $output['ddp_pages_user_info'] = [
      '#theme' => 'item_list',
      '#title' => $this->t(string: 'Cosas del usuario'),
      '#items' => $list,

    ];

    return $output;
  }

}
